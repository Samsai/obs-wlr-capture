# OBS WLR Screen capture Plugin

## ATTENTION: Consider using https://hg.sr.ht/~scoopta/wlrobs instead. This project isn't actively maintained.

## What is this?

This project is an OBS screen capture plugin for wlroots-based Wayland compositors. It uses the wlr-screencopy-v1
protocol to capture the screen and relays that information over to OBS. Note: this plugin will only work with
Wayland compositors that implement wlr-screencopy-v1. It will not, for example, work in GNOME or KDE at the
moment of writing (as far as I know, anyway). The hope is that either wlr-screencopy-v1 or a similar protocol
is standardized and adopted across the board.

This project was heavily inspired by [wf-recorder](https://github.com/ammen99/wf-recorder) and in fact many
parts of the plugin are directly copy-pasted from the wf-recorder sources.

## Disclaimers

I am not a particularly good C/C++ programmer and I have never written an OBS plugin before, so please keep
in mind that this software is of mediocre quality at best. This means that the plugin may be buggy, may only
work on my machine and may stop functioning due to OBS or Wayland updates. If you feel like you have reasonable
C/C++ skills and/or OBS plugin writing skills, I would appreciate pull requests and/or improved forks based on
this source code.

## Installation

To build the project, you will need Meson, ninja and the OBS and Wayland/wlroots headers.

You can build the project with the following command:

```sh
meson build
ninja -C build
```

To install the plugin, also run the following command:

```sh
ninja -C build install
```

By default the plugin will be installed into "~/.config/obs-studio/plugins/". To modify
the installation path, you can edit the "install.sh" file.

## Usage

To use the plugin, add the "wlr_capture_input" source to your scene in OBS. 
