
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <png.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/param.h>
#include <sys/wait.h>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <mutex>

#include <obs/obs-module.h>
#include <wayland-client-protocol.h>

#include "wlr-screencopy-unstable-v1-client-protocol.h"

OBS_DECLARE_MODULE()
OBS_MODULE_USE_DEFAULT_LOCALE("wlr-capture", "en_US")
MODULE_EXPORT const char *obs_module_description(void) {
    return "wlr-screencopy-unstable-v1 based screen capture for Wayland";
}

#define WLR_CAPTURE_DATA(voidptr) struct wlr_capture_data *data = (wlr_capture_data *) voidptr;

static struct wf_buffer {
	struct wl_buffer *wl_buffer;
	void *data;
	enum wl_shm_format format;
	int width, height, stride;
	bool y_invert;
} buffer;
static bool buffer_copy_done = false;

std::mutex copy_mutex;

wl_display *display;

static uint32_t output_width = 0;
static uint32_t output_height = 0;

struct wlr_capture_data {
    obs_source_t *source;
    wf_buffer *wf_buf; 
    bool *copy_done;
    gs_texture_t *texture;
};

struct format {
	enum wl_shm_format wl_format;
	bool is_bgr;
};

static struct wl_shm *shm = NULL;
static struct zwlr_screencopy_manager_v1 *screencopy_manager = NULL;
static struct wl_output *output = NULL;

static const struct format formats[] = {
	{WL_SHM_FORMAT_XRGB8888, true},
	{WL_SHM_FORMAT_ARGB8888, true},
	{WL_SHM_FORMAT_XBGR8888, false},
	{WL_SHM_FORMAT_ABGR8888, false},
};

static int backingfile(off_t size)
{
    char name[] = "/tmp/obs-wlr-capture-shared-XXXXXX";
    int fd = mkstemp(name);
    if (fd < 0) {
        return -1;
    }

    int ret;
    while ((ret = ftruncate(fd, size)) == EINTR) {
        // No-op
    }
    if (ret < 0) {
        close(fd);
        return -1;
    }

    unlink(name);
    return fd;
}

static struct wl_buffer *create_shm_buffer(uint32_t fmt,
    int width, int height, int stride, void **data_out)
{
    int size = stride * height;

    int fd = backingfile(size);
    if (fd < 0) {
        fprintf(stderr, "creating a buffer file for %d B failed: %m\n", size);
        return NULL;
    }

    void *data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (data == MAP_FAILED) {
        fprintf(stderr, "mmap failed: %m\n");
        close(fd);
        return NULL;
    }

    struct wl_shm_pool *pool = wl_shm_create_pool(shm, fd, size);
    close(fd);
    struct wl_buffer *buffer = wl_shm_pool_create_buffer(pool, 0, width, height,
        stride, fmt);
    wl_shm_pool_destroy(pool);

    *data_out = data;
    return buffer;
}

static void frame_handle_buffer(void *, struct zwlr_screencopy_frame_v1 *frame, uint32_t format,
    uint32_t width, uint32_t height, uint32_t stride)
{
    buffer.format = (wl_shm_format)format;
    buffer.width = width;
    buffer.height = height;
    buffer.stride = stride;

    if (!buffer.wl_buffer) {
        buffer.wl_buffer =
            create_shm_buffer(format, width, height, stride, &buffer.data);
    }

    if (buffer.wl_buffer == NULL) {
        fprintf(stderr, "failed to create buffer\n");
        exit(EXIT_FAILURE);
    }

    zwlr_screencopy_frame_v1_copy(frame, buffer.wl_buffer);
}

static void frame_handle_flags(void *data,
		struct zwlr_screencopy_frame_v1 *frame, uint32_t flags) {
	buffer.y_invert = flags & ZWLR_SCREENCOPY_FRAME_V1_FLAGS_Y_INVERT;
}

static void frame_handle_ready(void *data,
		struct zwlr_screencopy_frame_v1 *frame, uint32_t tv_sec_hi,
		uint32_t tv_sec_lo, uint32_t tv_nsec) {
	buffer_copy_done = true;
}

static void frame_handle_failed(void *data,
		struct zwlr_screencopy_frame_v1 *frame) {
	fprintf(stderr, "failed to copy frame\n");
	exit(EXIT_FAILURE);
}

static const struct zwlr_screencopy_frame_v1_listener frame_listener = {
	.buffer = frame_handle_buffer,
	.flags = frame_handle_flags,
	.ready = frame_handle_ready,
	.failed = frame_handle_failed,
};

static void handle_global(void *data, struct wl_registry *registry,
		uint32_t name, const char *interface, uint32_t version) {
	if (strcmp(interface, wl_output_interface.name) == 0 && output == NULL) {
		output = (wl_output *) wl_registry_bind(registry, name, &wl_output_interface, 1);
	} else if (strcmp(interface, wl_shm_interface.name) == 0) {
		shm = (wl_shm *) wl_registry_bind(registry, name, &wl_shm_interface, 1);
	} else if (strcmp(interface,
			zwlr_screencopy_manager_v1_interface.name) == 0) {
		screencopy_manager = (zwlr_screencopy_manager_v1 *) wl_registry_bind(registry, name,
			&zwlr_screencopy_manager_v1_interface, 1);
	}
}

static void handle_global_remove(void *data, struct wl_registry *registry,
		uint32_t name) {
	// Who cares?
}

static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = handle_global_remove,
};

static const char* wlr_capture_getname(void *unused) {
    UNUSED_PARAMETER(unused);
    return obs_module_text("wlr_capture_input");
}

static void *wlr_capture_create(obs_data_t *settings, obs_source_t *source) {
    // TODO: Create a plugin data context and pass it back to OBS
    
   display = wl_display_connect(NULL);
	if (display == NULL) {
		fprintf(stderr, "failed to create display: %m\n");
	}

	struct wl_registry *registry = wl_display_get_registry(display);
	wl_registry_add_listener(registry, &registry_listener, NULL);
	wl_display_dispatch(display);
	wl_display_roundtrip(display);
    
	if (shm == NULL) {
		fprintf(stderr, "compositor is missing wl_shm\n");
	}
	if (screencopy_manager == NULL) {
		fprintf(stderr, "compositor doesn't support wlr-screencopy-unstable-v1\n");
	}
	if (output == NULL) {
		fprintf(stderr, "no output available\n");
    } 

    struct wlr_capture_data *data = (wlr_capture_data *) bzalloc(sizeof(struct wlr_capture_data));
    data->source =  source;
    data->wf_buf = &buffer;
    data->copy_done = &buffer_copy_done;
    
    obs_enter_graphics();
    
    data->texture = gs_texture_create(0, 0, GS_BGRA, 1, NULL, GS_DYNAMIC);
    
    obs_leave_graphics();
    
    //wlr_capture_start();

    return data;
}

static void wlr_capture_destroy(void *vptr) {
    // TODO: Stop the capture and free all memory
    
    WLR_CAPTURE_DATA(vptr);

    while (!buffer_copy_done) {
        // busy wait
    }

    wl_buffer_destroy(buffer.wl_buffer);
    bfree(data);
}

static void wlr_capture_update(void *vptr, obs_data_t *settings) {
    // TODO: Update changed settings and restart capture
}

static uint32_t wlr_capture_width(void *vptr) {
    // TODO: Actually report width
    return buffer.width;
}

static uint32_t wlr_capture_height(void *vptr) {
    // TODO: Actually report height
    return buffer.height;
}

static obs_properties_t *wlr_capture_properties(void *vptr) {
    obs_properties_t *props = obs_properties_create();
    
    // TODO: Set up actual properties to be set by the user along with callbacks

    return props;
}

static void wlr_capture_defaults(obs_data_t *defaults) {
    // TODO: Set default values for properties
}

static void resize_texture(void *vptr) {
    WLR_CAPTURE_DATA(vptr);

    uint32_t t_width = gs_texture_get_width(data->texture);
    uint32_t t_height = gs_texture_get_height(data->texture);

    if (t_width != buffer.width || t_height != buffer.height) {
        gs_texture_destroy(data->texture);

        data->texture = gs_texture_create(buffer.width, buffer.height, GS_BGRA, 1, NULL, GS_DYNAMIC);
    }
}

static void wlr_capture_tick(void *vptr, float seconds) {
    UNUSED_PARAMETER(seconds);
    
    WLR_CAPTURE_DATA(vptr);

    // Try to lock, if we fail we skip the frame 
    bool locked = copy_mutex.try_lock(); 
    
    if (!locked) return;
    
    buffer_copy_done = false;

    struct zwlr_screencopy_frame_v1 *frame =
        zwlr_screencopy_manager_v1_capture_output(screencopy_manager, 1, output);
    zwlr_screencopy_frame_v1_add_listener(frame, &frame_listener, NULL);

    while (!buffer_copy_done && wl_display_dispatch(display) != -1) {
        // This space is intentionally left blank
    } 

    obs_enter_graphics();
    
    resize_texture(data);
    gs_texture_set_image(data->texture, (const uint8_t *) buffer.data, 1920 * 4, false);

    obs_leave_graphics();

    zwlr_screencopy_frame_v1_destroy(frame);
    
    copy_mutex.unlock();
}

static void wlr_capture_render(void *vptr, gs_effect_t *effect) {
    WLR_CAPTURE_DATA(vptr);

    effect = obs_get_base_effect(OBS_EFFECT_OPAQUE);
    
    if (!data->texture) return;
    
    gs_eparam_t *image = gs_effect_get_param_by_name(effect, "image");
	gs_effect_set_texture(image, data->texture);

	while (gs_effect_loop(effect, "Draw")) {
		gs_draw_sprite(data->texture, GS_FLIP_V, 0, 0);
	}
}

bool obs_module_load(void) {
    struct obs_source_info info = {};

    info.id = "wlr_capture_input";
    info.type = OBS_SOURCE_TYPE_INPUT;
    info.output_flags = OBS_SOURCE_VIDEO | OBS_SOURCE_CUSTOM_DRAW | OBS_SOURCE_DO_NOT_DUPLICATE;
    info.get_name = wlr_capture_getname;
    info.create = wlr_capture_create;
    info.destroy = wlr_capture_destroy;
    info.update = wlr_capture_update;
    info.get_width = wlr_capture_width;
    info.get_height = wlr_capture_height;
    info.get_properties = wlr_capture_properties;
    info.get_defaults = wlr_capture_defaults;
    info.video_tick = wlr_capture_tick;
    info.video_render = wlr_capture_render;

    obs_register_source(&info);

    return true;
}
